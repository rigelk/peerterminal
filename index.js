#!/usr/bin/env node

var youtubeDL = require('youtube-dl')
var Speaker = require('speaker')
var debug = require('debug')('yt-term')
var pcmAudio = require('./lib/pcm-audio')
var asciiVideo = require('./lib/ascii-video')

// command line options
var argv = require('minimist')(process.argv.slice(2), {
  alias: { l: 'link', i: 'invert', c: 'contrast', w: 'width', m: 'mute' },
  boolean: ['invert', 'mute']
})

if (argv.link) {
  // play from youtube link
  console.log('Playing:', argv.link)
  play(argv.link)
} else if (argv._.length > 0) {
  // search youtube and play the first result
  //console.log("search not implemented yet")
} else {
  printUsage()
}

function play (url) {
  youtubeDL.getInfo(url, function (err, info) {
    if (err) return console.error(err)
    playVideo(info)
  })
}

function playVideo (info) {
  // low resolution video only
  var videoItems = info.formats
    .filter(function (format) { return format.format_id === '240p' })

  // lowest resolution
  var video = videoItems[0]

  debug('Video format: %s [%s]', video.format_id, video.filesize)

  var videoInfo = {
    url: video.url,
    width: 426,
    height: video.height
  }

  var videoOptions = {
    fps: argv.fps /* || video.fps */ || 12,
    // TODO: width does not work well if video height is larger than terminal window
    width: argv.width || process.stdout.columns || 80,
    contrast: (argv.contrast || 50) * 2.55, // percent to byte
    invert: argv.invert
  }

  // play video as ascii
  asciiVideo(videoInfo, videoOptions)

  if (!argv.mute) playAudio(video.url)
}

function playAudio (url) {
  var speaker = new Speaker()

  var updateSpeaker = function (codec) {
    speaker.channels = codec.audio_details[2] === 'mono' ? 1 : 2
    speaker.sampleRate = parseInt(codec.audio_details[1].match(/\d+/)[0], 10)
  }

  // play audio
  pcmAudio(url).on('codecData', updateSpeaker).pipe(speaker)
}

function printUsage () {
  console.log('PeerTube Terminal ' + require('./package.json').version)
  console.log()
  console.log('Usage: peerterminal [options]')
  console.log()
  console.log('Options:')
  console.log()
  console.log('    -l, --link [url]         Use PeerTube link instead of searching')
  console.log('    -i, --invert             Invert colors, recommended on dark background')
  console.log('    -c, --contrast [percent] Adjust video contrast [default: 50]')
  console.log('    -w, --width [number]     ASCII video character width')
  console.log('    -m, --mute               Disable audio playback')
  console.log('    --fps [number]           Adjust playback frame rate')
  console.log()
  process.exit(0)
}

module.exports = function (args) {
  argv = require('minimist')(args.slice(1), {
    alias: { l: 'link', i: 'invert', c: 'contrast', w: 'width', m: 'mute' },
    boolean: ['invert', 'mute']
  })
  play(argv._[0])
}
