# PeerTerminal
[![license](https://img.shields.io/:license-MIT-blue.svg)](https://mvr.mit-license.org)

Stream YouTube videos as ascii art in the terminal!

## install

```
npm install -g peerterminal
```

Be sure to have [FFmpeg](https://www.ffmpeg.org) installed as well.

Ubuntu/Debian users should have ALSA installed as well:
```
sudo apt-get install libasound2-dev
```

## usage

PeerTerminal will play the first found search result:

```
peerterminal [options]
```

### options
```
-l, --link [url]         required.
-i, --invert             Invert colors, recommended on dark background
-c, --contrast [percent] Adjust video contrast [default: 50]
-w, --width [number]     ASCII video character width
-m, --mute               Disable audio playback
--fps [number]           Adjust playback frame rate
```

## todos

- [ ] Improve video playback (consistent frame interval)
- [ ] Video/Audio synchronization

## related

[ascii-pixels](https://github.com/mathiasvr/ascii-pixels)
[youtube-terminal](https://github.com/mathiasvr/youtube-terminal) (from which this project derives)

## license

MIT
